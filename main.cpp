#include <iostream>
#include <sys/mman.h>
#include <thread>
#include <chrono>

constexpr char bite{2};
int main(int argc, char **argv) {
    if (argc < 2 || argc > 2) {
        std::cerr << "Specify quantity of memory to eat in GiB\n";
        std::cerr << "usage: memeater 10\n";
        return EXIT_FAILURE;
    }

    // How much do we want to eat?
    int64_t eat_size{std::stoi(argv[1])};
    std::cerr << "eating " << eat_size << "GiB of memory\n";

    // Eat the memory, making sure to put each page into memory
    char* chomp = (char*)malloc(eat_size*1024*1024*1024);
    for (std::size_t eat_pos=0; eat_pos < eat_size*1024*1024*1024; eat_pos += 900) {
        chomp[eat_pos] = bite;
    }
    //mlock it all
    mlock(chomp, eat_size*1024*1024*1024);
    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(5));
        std::cout << "nom nom nom\n";
    }
    return EXIT_SUCCESS;
}
